/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guarani.domains;

/**
 *
 * @author Gustavo
 */
public class Company {
    
   private String cnpj;
   private String nome;
   private String email;
   private String moeda;

    public Company(String cnpj, String nome, String email, String moeda) {
        this.cnpj = cnpj;
        this.nome = nome;
        this.email = email;
        this.moeda = moeda;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }
 
    
   
    
}
